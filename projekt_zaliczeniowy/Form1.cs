﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace projekt_zaliczeniowy
{
    public partial class TłoPLaylista : Form
    {
        private List<Utwór> utworyNaLiście;
        private Utwór piosenka;
        public Form parent { get; set; }
        public TłoPLaylista()
        {
            InitializeComponent();
            
 
        
        }
        private string _wybranaLista;


        public string WybranaLista
        {
            get { return _wybranaLista; }
            set
            {
                _wybranaLista = value;
                var baza = new KontakZBaza();
                var utwory = baza.WybierzUtworyDanejPlaylisty(_wybranaLista);
                utworyNaLiście = utwory;
                foreach (var utwor in utwory)
                {
                    this.listBox_Utwory.Items.Add(utwor.tytuł);
                    listBox_Utwory.SelectedIndex = 0;
                }
            }
                

        }

        private void ŁadowanieDanych(object value)
        {
            if (listBox_Utwory.InvokeRequired)
            {
                ParameterizedThreadStart delegat = ŁadowanieDanych;
                this.Invoke(delegat, value);
            }
            else
            {
                var baza = new KontakZBaza();
                var utwory = baza.WybierzUtworyDanejPlaylisty(value.ToString());
                utworyNaLiście = utwory;
                foreach (var utwor in utwory)
                {
                    this.listBox_Utwory.Items.Add(utwor.tytuł);
                    listBox_Utwory.SelectedIndex = 0;
                }
            }
        }

        string[] plik, ścieżka_do_pliku;

        private void button1_Click(object sender, EventArgs e)
        {
            GRajAktualnyUtwór();
        }

        private void GRajAktualnyUtwór()
        {
            int index = listBox_Utwory.SelectedIndex;
            piosenka = utworyNaLiście.ElementAt(index);
            piosenka.PoczatekOdtwarzania += (sender) => { label2.Text = sender.autor; };
            piosenka.Odtwarzaj();
            Rysuj();
        }

        private void listView1_Utwory_Click(object sender, EventArgs e)
        {
            
            //player.play();
        }

        private void Rysuj()
        {
            var mojeSzczotki = new Brush[] { Brushes.BlueViolet, Brushes.Brown, Brushes.GreenYellow, Brushes.Indigo, Brushes.HotPink, Brushes.MediumVioletRed };
            var losak = new Random();
            int index = losak.Next(mojeSzczotki.Length);
            System.Drawing.Graphics graphics = this.CreateGraphics();
            System.Drawing.Rectangle rectangle = new System.Drawing.Rectangle(320, 73, 150, 150);
            graphics.FillRectangle(mojeSzczotki[index],rectangle);
            index = losak.Next(mojeSzczotki.Length);
            graphics.FillEllipse(mojeSzczotki[index], rectangle);
            graphics.DrawRectangle(System.Drawing.Pens.Red, rectangle);
        }

        private void DodajUtwor_Click(object sender, EventArgs e)
        {
            OpenFileDialog otwieranie_pliku = new OpenFileDialog();

             otwieranie_pliku.Filter = "Music files (*.wav)|*.wav";

            if (otwieranie_pliku.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Utwór piosenka = new Utwór(otwieranie_pliku.FileName);
                piosenka.Odtwarzaj();
                 
               
            }           
        }

        private void button2_Click(object sender, EventArgs e) //stop
        {
            piosenka.Zatrzymaj();
        }

        private void button3_Click(object sender, EventArgs e) // nastepny utwór
        {
            listBox_Utwory.SelectedIndex = (listBox_Utwory.SelectedIndex + 1) % listBox_Utwory.Items.Count;
            GRajAktualnyUtwór();

        }

        private void button4_Click(object sender, EventArgs e)  //poprzedni utwór
        {
            listBox_Utwory.SelectedIndex = (listBox_Utwory.SelectedIndex - 1) % listBox_Utwory.Items.Count;
            GRajAktualnyUtwór();
        }

        private void button6_Click(object sender, EventArgs e)  // powrót do menu
        {
            this.Hide();
            parent.Show();
          
        }

        //do zrobienia - głośniej
        private void button7_Click(object sender, EventArgs e) // zrobienie głośniej
        {
           
        }

    }
}

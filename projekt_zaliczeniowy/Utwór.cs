﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace projekt_zaliczeniowy
{
    
    class Utwór
    {

        private System.Media.SoundPlayer player = new System.Media.SoundPlayer();
        public string tytuł {get;set;}
        public string autor {get; set;}
        private string ścieżka_do_pliku;
        public delegate void MojeZdarzenie(Utwór sender);
        public MojeZdarzenie PoczatekOdtwarzania;
       

        public Utwór(string ścieżkaplikowa)
        {
            ścieżka_do_pliku = ścieżkaplikowa;
        }

        delegate void TypZdarzenia(object sender, string nazwaUtworu);

        public void Odtwarzaj()
        {
            
            player.SoundLocation = ścieżka_do_pliku;
            player.Play();
            if (PoczatekOdtwarzania != null)
                PoczatekOdtwarzania(this);

        }

        public void Zatrzymaj()
        {
            player.Stop();
          
        }


    }
}

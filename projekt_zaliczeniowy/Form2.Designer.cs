﻿namespace projekt_zaliczeniowy
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.ListBox_Playlist = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(128, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 17);
            this.label2.TabIndex = 2;
            // 
            // ListBox_Playlist
            // 
            this.ListBox_Playlist.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ListBox_Playlist.Font = new System.Drawing.Font("Comic Sans MS", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ListBox_Playlist.FormattingEnabled = true;
            this.ListBox_Playlist.ItemHeight = 19;
            this.ListBox_Playlist.Location = new System.Drawing.Point(384, 251);
            this.ListBox_Playlist.MultiColumn = true;
            this.ListBox_Playlist.Name = "ListBox_Playlist";
            this.ListBox_Playlist.Size = new System.Drawing.Size(273, 156);
            this.ListBox_Playlist.TabIndex = 10;
            this.ListBox_Playlist.Click += new System.EventHandler(this.ListBox_Playlist_Click);
            this.ListBox_Playlist.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ListBox_Playlist_KeyDown);
            this.ListBox_Playlist.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ListBox_Playlist_KeyUp);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.BackgroundImage = global::projekt_zaliczeniowy.Properties.Resources.music2;
            this.ClientSize = new System.Drawing.Size(679, 486);
            this.Controls.Add(this.ListBox_Playlist);
            this.Controls.Add(this.label2);
            this.Name = "Form2";
            this.Text = "GłówneMenu";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form2_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox ListBox_Playlist;
    }
}
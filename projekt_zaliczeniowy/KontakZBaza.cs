﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace projekt_zaliczeniowy
{
    class KontakZBaza
    {
        public List<Utwór> PobierzWszystkieUtwory()
        {
            SqlConnection polaczenie = new SqlConnection("Server=tcp:hidden,1433;Data Source=hidden;Initial Catalog=signalsAvnD5QAW0;Persist Security Info=False;User ID=hidden;Password=hidden;Pooling=False;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            SqlDataReader WynikPrzeszukania = null;

            try  
            {
                polaczenie.Open();


                SqlCommand polecenie1 = new SqlCommand("Select* from Utwory", polaczenie);
                
                WynikPrzeszukania = polecenie1.ExecuteReader();

                List<Utwór> WszystkieUtwory = new List<Utwór>();

                while (WynikPrzeszukania.Read())

                {
                    
                    Utwór nowyutwór = new Utwór (WynikPrzeszukania["ŚcieżkaDostępu"].ToString());
                    nowyutwór.tytuł = WynikPrzeszukania["Nazwa"].ToString();
                    nowyutwór.autor = WynikPrzeszukania["Autor"].ToString();
                   
                    WszystkieUtwory.Add(nowyutwór);
                }

                return WszystkieUtwory;
            }

            finally
            {
                // close the reader
                if (WynikPrzeszukania != null)
                {
                    WynikPrzeszukania.Close();
                }

                // 5. Close the connection
                if (polaczenie != null)
                {
                    polaczenie.Close();
                }
            }
        }

        //Wybrana Playlista

        public List<Utwór> WybierzUtworyDanejPlaylisty(string nazwaListy)
        {
            SqlConnection polaczenie = new SqlConnection("Server=tcp:hidden,1433;Data Source=hidden;Initial Catalog=signalsAvnD5QAW0;Persist Security Info=False;User ID=hidden;Password=hidden;Pooling=False;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            SqlDataReader WynikPrzeszukania = null;

            try
            {
                polaczenie.Open();

                string zapytanie = String.Format("Select Nazwa, Autor, ŚcieżkaDostępu from Utwory as U join UtworyNaListach as UList on U.ID=UList.ID_Utworu join Listy as L on UList.ID_LIsty=L.ID where Lista ='{0}'", nazwaListy);
                SqlCommand polecenie1 = new SqlCommand(zapytanie,polaczenie);

                WynikPrzeszukania = polecenie1.ExecuteReader();

                List<Utwór> WszystkieUtwory = new List<Utwór>();

                while (WynikPrzeszukania.Read())
                {

                    Utwór nowyutwór = new Utwór(WynikPrzeszukania["ŚcieżkaDostępu"].ToString());
                    nowyutwór.tytuł = WynikPrzeszukania["Nazwa"].ToString();
                    nowyutwór.autor = WynikPrzeszukania["Autor"].ToString();

                    WszystkieUtwory.Add(nowyutwór);
                }

                return WszystkieUtwory;
            }

            finally
            {
                // close the reader
                if (WynikPrzeszukania != null)
                {
                    WynikPrzeszukania.Close();
                }

                // 5. Close the connection
                if (polaczenie != null)
                {
                    polaczenie.Close();
                }
            }
        }

        }

    }


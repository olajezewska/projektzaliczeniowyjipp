﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace projekt_zaliczeniowy
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();

            DodajPlaylistyZBazy();

        }


        private void DodajPlaylistyZBazy()
        {
            SqlConnection polaczenie = new SqlConnection("Server=tcp:hidden,1433;Data Source=hidden;Initial Catalog=signalsAvnD5QAW0;Persist Security Info=False;User ID=hidden;Password=hidden;Pooling=False;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            SqlDataReader WynikPrzeszukania = null;

            try  //na wyjatki, tak wszystkie polaczenia do baz danych! bezpieczniej w tej sposob try catchem
            {

                polaczenie.Open();


                SqlCommand polecenie1 = new SqlCommand("Select* from Listy", polaczenie);
                
                WynikPrzeszukania = polecenie1.ExecuteReader();

                while (WynikPrzeszukania.Read())
                {
                    ListBox_Playlist.Items.Add(WynikPrzeszukania["lista"]);
                }
            }

            finally
            {
                // close the reader
                if (WynikPrzeszukania != null)
                {
                    WynikPrzeszukania.Close();
                }

                // 5. Close the connection
                if (polaczenie != null)
                {
                    polaczenie.Close();
                }
            }
        }

    

        private void ListBox_Playlist_Click(object sender, EventArgs e)
        {
            this.Hide();
            TłoPLaylista PLaylista = new TłoPLaylista() {WybranaLista = ListBox_Playlist.SelectedItem.ToString(), parent=this}; //inicjalizacja property
            
            PLaylista.ShowDialog();
            
        }

        private void ListBox_Playlist_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                this.Hide();
                TłoPLaylista PLaylista = new TłoPLaylista() { WybranaLista = ListBox_Playlist.SelectedItem.ToString(), parent = this }; //inicjalizacja property

                PLaylista.ShowDialog();
            }
        }

        private void ListBox_Playlist_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.Hide();
                TłoPLaylista PLaylista = new TłoPLaylista() { WybranaLista = ListBox_Playlist.SelectedItem.ToString(), parent = this }; //inicjalizacja property

                PLaylista.ShowDialog();
            }

        }

        private void Form2_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

      

       
    }
}
